BASES = en/package-cycle de/organisation
EXTS = eps png ps pdf
FILES = $(foreach ext,$(EXTS),$(foreach base,$(BASES),$(base).$(ext)))

.PHONY: all
all: $(FILES)

%.eps: %.dia
	dia -t eps $<

%.png: %.eps
	convert $< $@

%.pdf: %.eps
	epstopdf --outfile=$@ $<

.PHONY: clean
clean:
	rm -f $(FILES)
